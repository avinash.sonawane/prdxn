/**
 * Project          : PRDXN
 * Module           : Home
 * Source filename  : index.js
 * Author           : Avinash Sonawane <info2avisonawane1@gmail.com>
 */

/**
 * Controllers
 */

var mongoose = require('mongoose');
var Battle = require('../models/index');

module.exports = {
	list : function (req, res) {
		var qur = {'location' : {$ne : ""}};
		var response = [];
		Battle.find(qur)
			.select('location')
			.lean()
			.exec(function(err, result){
				if(err){
					res.status(200).send({"message" : "err", data: err});
				}else{
					result.forEach(function(r){
						response.push(r.location);
					});
					res.status(200).send({"message" : "ok", data: response});
				}
		});
	},
	count : function (req, res) {
		var qur = {'location' : {$ne : ""}};
		Battle.countDocuments({})
			.lean()
			.exec(function(err, result){
				if(err){
					res.status(200).send({"message" : "err", data: err});
				}else{
					res.status(200).send({"message" : "ok", data: result});
				}
		});
	},
	search : function(req, res){
		var qur = {$or : []};
		if(req.query.king){
			qur.$or.push({"attacker_king" : { $regex: new RegExp("^" + req.query.king.toLowerCase(), "i") }});
			qur.$or.push({"defender_king" : { $regex: new RegExp("^" + req.query.king.toLowerCase(), "i") }});
		}
		(req.query.location) ? qur.$or.push({"location" : { $regex: new RegExp("^" + req.query.location.toLowerCase(), "i") }}) : "";
		(req.query.type) ? qur.$or.push({"battle_type" : { $regex: new RegExp("^" + req.query.type.toLowerCase(), "i") }}) : "";
		(req.query.year) ? qur.$or.push({"year" : req.query.year}) : "";
		(req.query.number) ? qur.$or.push({"battle_number" : req.query.number}) : "";
		if(req.query.attacker){
			qur.$or.push({"attacker_1" : { $regex: new RegExp("^" + req.query.attacker.toLowerCase(), "i") }});
			qur.$or.push({"attacker_2" : { $regex: new RegExp("^" + req.query.attacker.toLowerCase(), "i") }});
			qur.$or.push({"attacker_3" : { $regex: new RegExp("^" + req.query.attacker.toLowerCase(), "i") }});
			qur.$or.push({"attacker_4" : { $regex: new RegExp("^" + req.query.attacker.toLowerCase(), "i") }});
		}
		if(req.query.defender){
			qur.$or.push({"defender_1" : { $regex: new RegExp("^" + req.query.defender.toLowerCase(), "i") }});
			qur.$or.push({"defender_2" : { $regex: new RegExp("^" + req.query.defender.toLowerCase(), "i") }});
			qur.$or.push({"defender_3" : { $regex: new RegExp("^" + req.query.defender.toLowerCase(), "i") }});
			qur.$or.push({"defender_4" : { $regex: new RegExp("^" + req.query.defender.toLowerCase(), "i") }});
		}
		if(req.query.size){
			qur.$or.push({"attacker_size" : req.query.size});
			qur.$or.push({"defender_size" : req.query.size});
		}
		if(req.query.commander){
			qur.$or.push({"attacker_commander" : { $regex: new RegExp("^" + req.query.commander.toLowerCase(), "i") }});
			qur.$or.push({"defender_commander" : { $regex: new RegExp("^" + req.query.commander.toLowerCase(), "i") }});
		}
		(req.query.region) ? qur.$or.push({"region" : { $regex: new RegExp("^" + req.query.region.toLowerCase(), "i") }}) : "";
		(qur.$or.length <= 0) ? qur={} : "";
		Battle.find(qur)
			.lean()
			.exec(function(err, result){
				if(err){
					res.status(200).send({"message" : "err", data: err});
				}else{
					res.status(200).send({"message" : "ok", data: result});
				}
		});

	},
	stats : function(req, res){
		var countAttacker = {};
		var countDeffender = {};
		var response = {"most_active" : {}};
		Battle.find({})
			.lean()
			.exec(function(err, result){
				if(err){
					res.status(200).send({"message" : "err", data: err});
				}else{
					result.forEach(function(i) { countAttacker[i.attacker_king] = (countAttacker[i.attacker_king]||0) + 1;});
					result.forEach(function(i) { countDeffender[i.defender_king] = (countDeffender[i.defender_king]||0) + 1;});
    				console.log(countAttacker);
    				console.log(countDeffender);
					res.status(200).send({"message" : "ok", data: response});
				}
		});
	}
}