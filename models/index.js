/**
 * Project          : PRDXN
 * Module           : Home Model
 * Source filename  : index.js
 * Author           : Avinash Sonawane <info2avisonawane1@gmail.com>
 */

/**
 * Model dependencies.
 */

var mongoose = require('mongoose');

var BattleSchema = new mongoose.Schema({
    name: {type : String, default : '', trim : true},
    year: {type: Number, default:0},
    battle_number: {type: Number, default:0},
    attacker_king: {type : String, default : '', trim : true},
    defender_king: {type : String, default : '', trim : true},
    attacker_1: {type : String, default : '', trim : true},
    attacker_2: {type : String, default : '', trim : true},
    attacker_3: {type : String, default : '', trim : true},
    attacker_4: {type : String, default : '', trim : true},
    defender_1: {type : String, default : '', trim : true},
    defender_2: {type : String, default : '', trim : true},
    defender_3: {type : String, default : '', trim : true},
    defender_4: {type : String, default : '', trim : true},
    attacker_outcome: {type : String, default : '', trim : true},
    battle_type: {type : String, default : '', trim : true},
    major_death: {type: Number, default:0},
    major_capture: {type : String, default : '', trim : true},
    attacker_size: {type: Number, default:0},
    defender_size: {type: Number, default:0},
    attacker_commander: {type : String, default : '', trim : true},
    defender_commander: {type : String, default : '', trim : true},
    summer: {type: Number, default:0},
    location: {type : String, default : '', trim : true},
    major_capture: {type : String, default : '', trim : true},
    note: {type : String, default : '', trim : true},
});

module.exports = mongoose.model('Battle', BattleSchema);

