/**
 * Project          : PRDXN
 * Module           : Home
 * Source filename  : index.js
 * Author           : Avinash Sonawane <info2avisonawane1@gmail.com>
 */

/**
 * Routes
 */

var express = require('express');
var router = express.Router();
var controllers = require('../controllers/index.js');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET list of battles. */
router.get('/list', function(req, res, next) {
  controllers.list(req, res);
});

/* GET count of battles. */
router.get('/count', function(req, res, next) {
  controllers.count(req, res);
});

/* GET search data of battles. */
router.get('/search', function(req, res, next) {
  controllers.search(req, res);
});

/* GET search data of battles. */
router.get('/stats', function(req, res, next) {
  controllers.stats(req, res);
});



module.exports = router;
